<?php

/**
 * Created by PhpStorm.
 * Author: aaasayok
 * Email: aaasayok@gmail.com
 * Date: 2016/11/27-23:03
 */
namespace Patterns\Singleton;

/**
 * 单列模式  singleton
 *
 * 应用场合：有些对象只需要一个就足够了，  如读取同一份的配置文件
 * 作用： 保证整个应用程序中某个实例有且只有一个
 * 类型： 恶汉模式  懒汉模式
 *
 * Class Singleton
 * @package Patterns\Singleton
 */
class Singleton
{
    private static $instances;

    //1 构造方法私有化，不允许外部直接实例化
    private function __construct()
    {
    }

    public static function getInstances()
    {
        if (null === self::$instances) {
            self::$instances = new Singleton();
        }

        return self::$instances;
    }

}