<?php
/**
 * Created by PhpStorm.
 * Author: aaasayok
 * Email: aaasayok@gmail.com
 * Date: 2016/11/27-23:04
 */

require __DIR__ . '/../vendor/autoload.php';

use Patterns\Singleton\Singleton;

$singleton = Singleton::getInstances();

$singletonTest = Singleton::getInstances();

//$singleton2 = new Singleton();
//var_dump($singleton2);

var_dump($singleton);

var_dump($singletonTest);